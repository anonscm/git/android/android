package de.tarent.android.tasks;

import java.text.SimpleDateFormat;
import java.util.Date;

import de.tarent.android.tasks.Tasks.Task;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class TaskViewer extends Activity {
	/** The index of the note column */

	private Uri mUri;
	private Cursor mCursor;
	private TextView mTaskDescr;
	private TextView mTaskTitle;
	private TextView mStartEnd;
	private TextView mPrio;
	private TextView mRepeat;

	private String taskTitle;
	private String taskDescr;
	private long startDateMillis;
	private long endDateMillis;
	private String prio;
	private String repeat;
	private String startEndDisplay;
	private String startDate;
	private String endDate;

	private static final String[] PROJECTION = new String[] { Task._ID,
			Task.TASK_TITLE, Task.TASK, Task.PRIO, Task.REPEAT,
			Task.START_DATE, Task.END_DATE,

	};

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Set Layout
		setContentView(R.layout.view_task);
		mTaskDescr = (TextView) findViewById(R.id.view_task_descr);
		mTaskTitle = (TextView) findViewById(R.id.view_task_title);
		mStartEnd = (TextView) findViewById(R.id.view_start_end);
		mPrio = (TextView) findViewById(R.id.view_prio);
		mRepeat = (TextView) findViewById(R.id.view_repeat);

		final Intent intent = getIntent();

		// Do some setup based on the action being performed.
		final String action = intent.getAction();
		if (Intent.ACTION_VIEW.equals(action)) {
			System.out.println("ACTION View");
		}

		mUri = intent.getData();

		// Get the note!
		mCursor = managedQuery(mUri, PROJECTION, null, null, null);

		getTaskContent();
		populateTextViews();

		final Button button = (Button) findViewById(R.id.widget45);
		button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Perform action on clicks
				editTask();
			}
		});

	}

	private void getTaskContent() {

		// If we didn't have any trouble retrieving the data, it is now
		// time to get at the stuff.
		if (mCursor != null) {
			// Make sure we are at the one and only row in the cursor.
			mCursor.moveToFirst();
			int taskTitleIndex = mCursor.getColumnIndex(Task.TASK_TITLE);
			int taskDescrIndex = mCursor.getColumnIndex(Task.TASK);
			int startDateIndex = mCursor.getColumnIndex(Task.START_DATE);
			int endDateIndex = mCursor.getColumnIndex(Task.END_DATE);
			int prioIndex = mCursor.getColumnIndex(Task.PRIO);
			int repeatIndex = mCursor.getColumnIndex(Task.REPEAT);

			taskTitle = mCursor.getString(taskTitleIndex);
			taskDescr = mCursor.getString(taskDescrIndex);
			startDateMillis = mCursor.getLong(startDateIndex);
			endDateMillis = mCursor.getLong(endDateIndex);
			prio = Long.toString(mCursor.getLong(prioIndex));
			repeat = mCursor.getString(repeatIndex);

			SimpleDateFormat d = new SimpleDateFormat("dd.MM.yyyy, HH:mm");

			startDate = "" + d.format(new Date(startDateMillis));
			endDate = d.format(new Date(endDateMillis));
		}
	}

	private void populateTextViews() {

		startEndDisplay = startDate + " - " + endDate;
		// This is a little tricky: we may be resumed after previously being
		// paused/stopped. We want to put the new text in the text view,
		// but leave the user where they were (retain the cursor position
		// etc). This version of setText does that for us.
		mTaskTitle.setText(taskTitle);
		mTaskDescr.setText(taskDescr);
		mStartEnd.setText(startEndDisplay);
		mPrio.setText(prio);
		mRepeat.setText(repeat);

	}

	/* Creates the menu items */
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 1, 0, "Taskübersicht").setIcon(
				android.R.drawable.ic_menu_view);
		menu.add(0, 2, 0, "Editieren").setIcon(android.R.drawable.ic_menu_edit);

		return true;
	}

	/* Handles item selections */
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case 1:
			this.finish();
			return true;
		case 2:
			editTask();
			return true;
		}
		return false;
	}

	private void editTask() {
		Toast.makeText(TaskViewer.this, "Editiere Task", Toast.LENGTH_SHORT)
				.show();
		Intent myIntent = new Intent(TaskViewer.this, TaskEditor.class);
		// Set the Content URI
		myIntent.setData(getIntent().getData());
		myIntent.setAction(Intent.ACTION_EDIT);
		TaskViewer.this.startActivity(myIntent);

		// end activity
		this.finish();
	}
}
