package de.tarent.android.tasks;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ViewFlipper;
import de.tarent.android.tasks.Tasks.Task;

public class TaskEditor extends Activity {
	private static final String TAG = "Tasks";

	ViewFlipper flipper;

	private static final String[] PROJECTION = new String[] { Task._ID,
			Task.TASK_TITLE, Task.TASK, Task.START_DATE, Task.END_DATE,
			Task.PRIO, Task.REPEAT, };

	private Button mPickDate;
	private Button mPickTime;
	private Button mEndPickDate;
	private Button mEndPickTime;
	private Button mSaveTask;
	private Uri mUri;
	private Cursor mCursor;
	private int mId;

	private String taskTitle;
	private String taskDescr;
	private long startDateMillis;
	private long endDateMillis;
	private int prio;
	private String repeat;

	private EditText mTaskTitle;
	private EditText mTaskDescr;
	private Spinner mPrio;
	private Spinner mRepeat;

	private int mRepeatCount;
	private int mYear;
	private int mMonth;
	private int mDay;
	private int mHour;
	private int mMinute;
	private int mEndYear;
	private int mEndMonth;
	private int mEndDay;
	private int mEndHour;
	private int mEndMinute;

	static final int TIME_DIALOG_ID = 0;
	static final int DATE_DIALOG_ID = 1;
	static final int END_TIME_DIALOG_ID = 2;
	static final int END_DATE_DIALOG_ID = 3;

	// the callback received when the user "sets" the date in the dialog
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDisplay();
		}
	};

	// the callback received when the user "sets" the time in the dialog
	private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			mHour = hourOfDay;
			mMinute = minute;
			updateDisplay();
		}
	};

	private DatePickerDialog.OnDateSetListener mEndDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mEndYear = year;
			mEndMonth = monthOfYear;
			mEndDay = dayOfMonth;
			updateDisplay();
			checkDates();
		}
	};



	// the callback received when the user "sets" the time in the dialog
	private TimePickerDialog.OnTimeSetListener mEndTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			mEndHour = hourOfDay;
			mEndMinute = minute;
			updateDisplay();
		}
	};

	/**
	 * Check if EndDate is before Startdate
	 */
	private void checkDates() {
		if (mYear > mEndYear ){
			wrongDateAlert();
		} else if (mMonth > mEndMonth){
			wrongDateAlert();
		} else if (mDay > mEndDay) {
			wrongDateAlert();
		}
	}
	
	private void wrongDateAlert() {
		   new AlertDialog.Builder(this)
		      .setMessage("Das Enddatum kann nicht vor dem Startdatum sein.")
		      .setTitle("Datum Fehlerhaft")
		      .setCancelable(true)
		      .setNeutralButton(android.R.string.ok,
		         new DialogInterface.OnClickListener() {
		         public void onClick(DialogInterface dialog, int whichButton){}
		         })
		      .show();
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tab_holder);
		// set Tabbed View
		flipper = (ViewFlipper) findViewById(R.id.flipper);
		setListeners();

		initializeWidgets();

		final Intent intent = getIntent();

		// Do some setup based on the action being performed.
		final String action = intent.getAction();
		if (Intent.ACTION_EDIT.equals(action)) {
			// Requested to edit: set the data being edited.
			mUri = intent.getData();

		} else if (Intent.ACTION_INSERT.equals(action)) {
			// Requested to insert: create a new entry
			// in the container.
			mUri = getContentResolver().insert(intent.getData(), null);

			// If we were unable to create a new note, then just finish
			// this activity. A RESULT_CANCELED will be sent back to the
			// original activity if they requested a result.
			if (mUri == null) {
				Log.e(TAG, "Failed to insert new task into "
						+ getIntent().getData());
				finish();
				return;
			}

			// The new entry was created, so assume all will end well and
			// set the result to be returned.
			setResult(RESULT_OK, (new Intent()).setAction(mUri.toString()));
		} else {
			// Whoops, unknown action! Bail.
			Log.e(TAG, "Unknown action, exiting");
			finish();
			return;
		}

		// Get the Task
		getTasksData();

		// if action is Edit, fill the widgets with the tasks content
		if (Intent.ACTION_EDIT.equals(action)) {
			populateFields();
		}

	}
	
	
	//intercept back-button
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
	    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
	        // do something on back.
	    	System.out.println("BACK-KEY pressed. Delete blank task if Intent is ACTION_INSERT.");
			final Intent intent = getIntent();
			// if action ist "insert", the blank note can be deleted because it
			// has no content
			if (Intent.ACTION_INSERT.equals(intent.getAction())) {
				getContentResolver().delete(mUri, null, null);

			}

			this.finish();
			
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}

	/**
	 * Initialize the widgets from the layout-xml
	 */
	private void initializeWidgets() {

		// Get the EditText and Button References
		mTaskDescr = (EditText) findViewById(R.id.task_descr);
		mTaskTitle = (EditText) findViewById(R.id.task_title);

		// Fill Spinners with the needed content
		mPrio = (Spinner) findViewById(R.id.prio_spinner);
		ArrayAdapter adapter = ArrayAdapter.createFromResource(TaskEditor.this,
				R.array.prios, android.R.layout.simple_spinner_item);
		adapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mPrio.setAdapter(adapter);

		mRepeat = (Spinner) findViewById(R.id.wdhSpinner);
		ArrayAdapter adapter2 = ArrayAdapter.createFromResource(
				TaskEditor.this, R.array.wdh,
				android.R.layout.simple_spinner_item);
		adapter2
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mRepeat.setAdapter(adapter2);

		// Date- and timebuttons
		mPickDate = (Button) findViewById(R.id.pickDate);
		mEndPickDate = (Button) findViewById(R.id.endPickDate);
		mEndPickTime = (Button) findViewById(R.id.endPickTime);
		mPickTime = (Button) findViewById(R.id.pickTime);

		// save button
		mSaveTask = (Button) findViewById(R.id.save_button);
		mSaveTask.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				SaveTask();
			}
		});

		TimeView();

	}

	/**
	 * Get Data from the content Provider
	 */
	private void getTasksData() {
		mCursor = managedQuery(mUri, PROJECTION, null, null, null);
		mCursor.moveToFirst();

		// Get Column Index by Column-Name
		int taskId = mCursor.getColumnIndex("_id");
		int taskTitleIndex = mCursor.getColumnIndex(Task.TASK_TITLE);
		int taskDescrIndex = mCursor.getColumnIndex(Task.TASK);
		int startDateIndex = mCursor.getColumnIndex(Task.START_DATE);
		int endDateIndex = mCursor.getColumnIndex(Task.END_DATE);
		int prioIndex = mCursor.getColumnIndex(Task.PRIO);
		int repeatIndex = mCursor.getColumnIndex(Task.REPEAT);

		// get Id
		mId = mCursor.getInt(taskId);
		// initialize varibles with values from the content provider
		taskTitle = mCursor.getString(taskTitleIndex);
		taskDescr = mCursor.getString(taskDescrIndex);
		startDateMillis = mCursor.getLong(startDateIndex);
		endDateMillis = mCursor.getLong(endDateIndex);
		prio = mCursor.getInt(prioIndex);
		repeat = mCursor.getString(repeatIndex);

		// get time information
		Calendar startDateCalendar = new GregorianCalendar();
		startDateCalendar.setTimeInMillis(startDateMillis);
		mYear = startDateCalendar.get(Calendar.YEAR);
		mMonth = startDateCalendar.get(Calendar.MONTH);
		mDay = startDateCalendar.get(Calendar.DATE);
		mHour = startDateCalendar.get(Calendar.HOUR_OF_DAY);
		mMinute = startDateCalendar.get(Calendar.MINUTE);

		Calendar endDateCalendar = new GregorianCalendar();
		endDateCalendar.setTimeInMillis(endDateMillis);
		mEndYear = endDateCalendar.get(Calendar.YEAR);
		mEndMonth = endDateCalendar.get(Calendar.MONTH);
		mEndDay = endDateCalendar.get(Calendar.DATE);
		mEndHour = endDateCalendar.get(Calendar.HOUR_OF_DAY);
		mEndMinute = endDateCalendar.get(Calendar.MINUTE);
		updateDisplay();

		// determine which count the selection for repeat has
		if (repeat.equals("nie")) {
			mRepeatCount = 0;
		} else if (repeat.equals("Tag")) {
			mRepeatCount = 1;
		} else if (repeat.equals("Woche")) {
			mRepeatCount = 2;
		} else if (repeat.equals("Monat")) {
			mRepeatCount = 3;
		} else if (repeat.equals("Jahr")) {
			mRepeatCount = 4;
		}

	}

	/**
	 * Fill Widgets with content from the CP
	 **/
	private void populateFields() {
		mTaskTitle.setText(taskTitle);
		mTaskDescr.setText(taskDescr);
		mPrio.setSelection(prio - 1);
		mRepeat.setSelection(mRepeatCount);

	}

	/* Creates the menu items */
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 1, 0, "Save").setIcon(android.R.drawable.ic_menu_save);
		menu.add(0, 2, 0, "Discard").setIcon(
				android.R.drawable.ic_menu_close_clear_cancel);

		return true;
	}

	/* Handles item selections */
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case 1:
			SaveTask();
			return true;
		case 2:
			final Intent intent = getIntent();
			// if action ist "insert", the blank note can be deleted because it
			// has no content
			if (Intent.ACTION_INSERT.equals(intent.getAction())) {
				getContentResolver().delete(mUri, null, null);

			}

			this.finish();
			return true;
		}
		return false;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		// identify which dialog was requested
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, mDateSetListener, mYear, mMonth,
					mDay);
		case TIME_DIALOG_ID:
			return new TimePickerDialog(this, mTimeSetListener, mHour, mMinute,
					true);
		case END_DATE_DIALOG_ID:
			return new DatePickerDialog(this, mEndDateSetListener, mEndYear,
					mEndMonth, mEndDay);
		case END_TIME_DIALOG_ID:
			return new TimePickerDialog(this, mEndTimeSetListener, mEndHour,
					mEndMinute, true);
		}
		return null;

	}

	/**
	 * Update date- and timebuttons
	 */
	private void updateDisplay() {
		mPickDate.setText(new StringBuilder()
				// Month is 0 based so add 1
				.append(mDay).append("-").append(mMonth + 1).append("-")
				.append(mYear).append(" "));

		mPickTime.setText(new StringBuilder().append(pad(mHour)).append(":")
				.append(pad(mMinute)));

		mEndPickDate.setText(new StringBuilder()
				// Month is 0 based so add 1
				.append(mEndDay).append("-").append(mEndMonth + 1).append("-")
				.append(mEndYear).append(" "));

		mEndPickTime.setText(new StringBuilder().append(pad(mEndHour)).append(
				":").append(pad(mEndMinute)));

	}

	private static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	private void TimeView() {

		// add a click listener to the button
		mPickDate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog(DATE_DIALOG_ID);
			}
		});

		// get the current date
		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);

		// add a click listener to the button
		mPickTime.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog(TIME_DIALOG_ID);
			}
		});

		// get the current time
		mHour = c.get(Calendar.HOUR_OF_DAY);
		mMinute = c.get(Calendar.MINUTE);

		// add a click listener to the button
		mEndPickDate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog(END_DATE_DIALOG_ID);
			}
		});

		// get the current date
		final Calendar c_end = Calendar.getInstance();
		mEndYear = c_end.get(Calendar.YEAR);
		mEndMonth = c_end.get(Calendar.MONTH);
		mEndDay = c_end.get(Calendar.DAY_OF_MONTH);

		// add a click listener to the button
		mEndPickTime.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog(END_TIME_DIALOG_ID);
			}
		});

		// display the current date
		updateDisplay();
	}

	/**
	 * set listener for the different tabs
	 */
	private void setListeners() {

		TextView tab1 = (TextView) findViewById(R.id.tab1);
		tab1.setOnClickListener(onClickListener);
		TextView tab2 = (TextView) findViewById(R.id.tab2);
		tab2.setOnClickListener(onClickListener);

	}

	private OnClickListener onClickListener = new OnClickListener() {

		public void onClick(final View v) {
			switch (v.getId()) {
			// if tab 1 is clicked...
			case R.id.tab1:

				flipper.setInAnimation(inFromLeftAnimation());
				flipper.setOutAnimation(outToRightAnimation());
				flipper.showPrevious();

				System.out.println("tab1");

				break;

			case R.id.tab2:

				System.out.println("tab2");

				flipper.setInAnimation(inFromRightAnimation());
				flipper.setOutAnimation(outToLeftAnimation());
				flipper.showNext();

				break;
			}
		}
	};

	private Animation inFromRightAnimation() {

		Animation inFromRight = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, +1.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f);
		inFromRight.setDuration(500);
		inFromRight.setInterpolator(new AccelerateInterpolator());
		return inFromRight;
	}

	private Animation outToLeftAnimation() {
		Animation outtoLeft = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, -1.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f);
		outtoLeft.setDuration(500);
		outtoLeft.setInterpolator(new AccelerateInterpolator());
		return outtoLeft;
	}

	private Animation inFromLeftAnimation() {
		Animation inFromLeft = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, -1.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f);
		inFromLeft.setDuration(500);
		inFromLeft.setInterpolator(new AccelerateInterpolator());
		return inFromLeft;
	}

	private Animation outToRightAnimation() {
		Animation outtoRight = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, +1.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f);
		outtoRight.setDuration(500);
		outtoRight.setInterpolator(new AccelerateInterpolator());
		return outtoRight;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(Task._ID, mId);
	}

	/**
	 * Save the Task to the Content-Provider
	 */
	private void SaveTask() {
		ContentValues values = new ContentValues();

		Toast toast = Toast.makeText(TaskEditor.this,
				"Ihr Task wird gespeichert", Toast.LENGTH_SHORT);
		toast.show();

		// Get startdate in Millis
		long startDate = new GregorianCalendar(mYear, mMonth, mDay, mHour,
				mMinute).getTimeInMillis();
		long endDate = new GregorianCalendar(mEndYear, mEndMonth, mEndDay,
				mEndHour, mEndMinute).getTimeInMillis();

		values.put("task_title", mTaskTitle.getText().toString());
		values.put("task", mTaskDescr.getText().toString());
		values.put("prio", mPrio.getSelectedItemPosition());
		values.put("repeat", mRepeat.getSelectedItem().toString());
		values.put("startdate", startDate);
		values.put("enddate", endDate);

		getContentResolver().update(mUri, values, null, null);

		this.finish();

	}

}