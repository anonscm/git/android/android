package de.tarent.android.tasks;

import android.net.Uri;
import android.provider.BaseColumns;

public final class Tasks {
	
	public static final String AUTHORITY = "de.tarent.provider.Tasks";
	private Tasks() {}//kann nicht instanziiert werden
	
	//Tasks table
	public static final class Task implements BaseColumns {
		
		private Task(){}
		
		public static final Uri CONTENT_URI = Uri
				.parse("content://" + AUTHORITY + "/tasks");
		
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.google.task";

        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.google.task";

		public static final String DEFAULT_SORT_ORDER = "modified DESC";
		
		public static final String TASK = "task";
		
		public static final String TASK_TITLE = "task_title";
		
		public static final String CREATED_DATE = "created";
		
		public static final String START_DATE = "startdate";
		
		public static final String END_DATE = "enddate";
		
		public static final String PRIO = "prio";
		
		public static final String REPEAT = "repeat";		
		
		public static final String MODIFIED_DATE = "modified";
	}
}
