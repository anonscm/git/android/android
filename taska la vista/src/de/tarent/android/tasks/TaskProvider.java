package de.tarent.android.tasks;

import java.util.HashMap;

import de.tarent.android.tasks.Tasks.Task;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import de.tarent.android.tasks.Tasks;

public class TaskProvider extends ContentProvider {

	private static final String DATABASE_NAME = "tasks.db";
	private static final int DATABASE_VERSION = 2;
	private static final String TASKS_TABLE_NAME = "tasks";
	private Databasehelper mOpenHelper;

	private static HashMap<String, String> sTasksProjectionMap;

	private static final int TASKS = 1;
	private static final int TASK_ID = 2;

	private static final UriMatcher sUriMatcher;

	static {
		sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		sUriMatcher.addURI(Tasks.AUTHORITY, "tasks", TASKS);
		sUriMatcher.addURI(Tasks.AUTHORITY, "tasks/#", TASK_ID);

		sTasksProjectionMap = new HashMap<String, String>();
		sTasksProjectionMap.put(Task._ID, Task._ID);
		sTasksProjectionMap.put(Task.TASK_TITLE, Task.TASK_TITLE);
		sTasksProjectionMap.put(Task.TASK, Task.TASK);
		sTasksProjectionMap.put(Task.START_DATE, Task.START_DATE);
		sTasksProjectionMap.put(Task.END_DATE, Task.END_DATE);
		sTasksProjectionMap.put(Task.REPEAT, Task.REPEAT);
		sTasksProjectionMap.put(Task.CREATED_DATE, Task.CREATED_DATE);
		sTasksProjectionMap.put(Task.MODIFIED_DATE, Task.MODIFIED_DATE);
		sTasksProjectionMap.put(Task.PRIO, Task.PRIO);

	}

	@Override
	public int delete(Uri uri, String where, String[] whereArgs) {
		SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		int count;
		switch (sUriMatcher.match(uri)) {
		case TASKS:
			count = db.delete(TASKS_TABLE_NAME, where, whereArgs);
			break;
		case TASK_ID:
			String taskId = uri.getPathSegments().get(1);
			count = db.delete(TASKS_TABLE_NAME,
					Task._ID
							+ "="
							+ taskId
							+ (!TextUtils.isEmpty(where) ? " AND (" + where
									+ ')' : ""), whereArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}

		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	@Override
	public String getType(Uri uri) {
		switch (sUriMatcher.match(uri)) {
		case TASKS:
			return Task.CONTENT_TYPE;
		case TASK_ID:
			return Task.CONTENT_ITEM_TYPE;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues initialValues) {
		if (sUriMatcher.match(uri) != TASKS) {
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
		ContentValues values;
		if (initialValues != null)
			values = new ContentValues(initialValues);
		else
			values = new ContentValues();

		Long now = Long.valueOf(System.currentTimeMillis());

		// Make sure that the fields are all set
		if (values.containsKey(Tasks.Task.CREATED_DATE) == false)
			values.put(Tasks.Task.CREATED_DATE, now);

		if (values.containsKey(Tasks.Task.MODIFIED_DATE) == false)
			values.put(Tasks.Task.MODIFIED_DATE, now);

		if (values.containsKey(Tasks.Task.START_DATE) == false)
			values.put(Tasks.Task.START_DATE, now);

		if (values.containsKey(Tasks.Task.END_DATE) == false)
			values.put(Tasks.Task.END_DATE, now);

		if (values.containsKey(Tasks.Task.PRIO) == false)
			values.put(Tasks.Task.PRIO, now);

		if (values.containsKey(Tasks.Task.REPEAT) == false)
			values.put(Tasks.Task.REPEAT, now);

		if (values.containsKey(Tasks.Task.TASK) == false)
			values.put(Tasks.Task.TASK, "untitled");

		if (values.containsKey(Tasks.Task.TASK_TITLE) == false) {
			Resources r = Resources.getSystem();
			values.put(Tasks.Task.TASK_TITLE, r
					.getString(android.R.string.untitled));
		}

		SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		long rowId = db.insert(TASKS_TABLE_NAME, Task.TASK, values);
		if (rowId > 0) {
			Uri taskUri = ContentUris.withAppendedId(Tasks.Task.CONTENT_URI,
					rowId);
			getContext().getContentResolver().notifyChange(taskUri, null);
			return taskUri;
		}
		throw new SQLException("Failed to insert row into " + uri);
	}

	@Override
	public boolean onCreate() {
		mOpenHelper = new Databasehelper(getContext());
		return true;
	}

	private static class Databasehelper extends SQLiteOpenHelper {
		Databasehelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("CREATE TABLE " + TASKS_TABLE_NAME + " (" + Task._ID
					+ " INTEGER PRIMARY KEY," + Task.TASK_TITLE + " TEXT,"
					+ Task.TASK + " TEXT," + Task.START_DATE + " LONG,"
					+ Task.END_DATE + " LONG," + Task.CREATED_DATE
					+ " INTEGER," + Task.MODIFIED_DATE + " INTEGER,"
					+ Task.PRIO + " Integer," + Task.REPEAT + " TEXT" + ");");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldver, int newver) {
			// destroy the old version
			db.execSQL("DROP TABLE IF EXISTS tasks");
			onCreate(db);
		}

	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {

		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

		switch (sUriMatcher.match(uri)) {
		case TASKS:
			qb.setTables(TASKS_TABLE_NAME);
			qb.setProjectionMap(sTasksProjectionMap);
			break;

		case TASK_ID:
			qb.setTables(TASKS_TABLE_NAME);
			qb.setProjectionMap(sTasksProjectionMap);
			qb.appendWhere(Task._ID + "=" + uri.getPathSegments().get(1));
			break;

		default:
			throw new IllegalArgumentException("unknown URI " + uri);
		}

		// If no sortorder is specified, use the default
		String orderBy;
		if (TextUtils.isEmpty(sortOrder)) {
			orderBy = Tasks.Task.DEFAULT_SORT_ORDER;
		} else {
			orderBy = sortOrder;
		}

		// get the database and run the query
		SQLiteDatabase db = mOpenHelper.getReadableDatabase();
		Cursor c = qb.query(db, projection, selection, selectionArgs, null,
				null, orderBy);

		// Tell cursor what uri to watch, so it knows when its source data
		// changes
		c.setNotificationUri(getContext().getContentResolver(), uri);
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String where,
			String[] whereArgs) {
		SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		int count;
		switch (sUriMatcher.match(uri)) {
		case TASKS:
			count = db.update(TASKS_TABLE_NAME, values, where, whereArgs);

		case TASK_ID:
			String taskId = uri.getPathSegments().get(1);
			count = db.update(TASKS_TABLE_NAME, values,
					Task._ID
							+ "="
							+ taskId
							+ (!TextUtils.isEmpty(where) ? " AND (" + where
									+ ')' : ""), whereArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);

		}

		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

}
