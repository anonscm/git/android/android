package de.tarent.android.tasks;

import android.app.ListActivity;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import de.tarent.android.tasks.Tasks.Task;

public class TaskaLaVista extends ListActivity {
	private static final String TAG = "Tasks";

	protected static final int CONTEXT_VIEW_TASK = Menu.FIRST;
	protected static final int CONTEXT_EDIT_TASK = Menu.FIRST + 1;
	protected static final int CONTEXT_DELETE_TASK = Menu.FIRST + 2;

	private static final String[] PROJECTION = new String[] { Task._ID, // 0
			Task.TASK, Task.TASK_TITLE };

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		// If no data was given in the intent (because we were started
		// as a MAIN activity), then use our default content provider.
		Intent intent = getIntent();
		if (intent.getData() == null) {
			intent.setData(Task.CONTENT_URI);
		}

		// Inform the list we provide context menus for items
		getListView().setOnCreateContextMenuListener(this);

		// Perform a managed query. The Activity will handle closing and
		// requerying the cursor
		// when needed.
		Cursor cursor = managedQuery(getIntent().getData(), PROJECTION, null,
				null, Task.DEFAULT_SORT_ORDER);

		// Used to map notes entries from the database to views
		SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
				R.layout.row, cursor, new String[] { Task.TASK_TITLE },
				new int[] { android.R.id.text1 });
		setListAdapter(adapter);

	}

	/* Creates the menu items */
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 1, 0, "Add").setIcon(android.R.drawable.ic_input_add);
		menu.add(0, 2, 0, "Quit").setIcon(
				android.R.drawable.ic_menu_close_clear_cancel);
		return true;
	}

	/* Handles item selections */
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 1:
			Toast view_toast = Toast.makeText(TaskaLaVista.this,
					"Neuer Task wird angelegt", Toast.LENGTH_LONG);
			view_toast.show();

			Intent myIntent1 = new Intent(TaskaLaVista.this, TaskEditor.class);
			myIntent1.setAction(Intent.ACTION_INSERT);
			myIntent1.setData(getIntent().getData());
			TaskaLaVista.this.startActivity(myIntent1);

			return true;
		case 2:
			System.exit(1);
			return true;
		}
		return false;
	}

	public void onCreateContextMenu(ContextMenu menu, View view,
			ContextMenuInfo menuInfo) {
		menu.add(0, CONTEXT_VIEW_TASK, 0, "Task ansehen");
		menu.add(0, CONTEXT_EDIT_TASK, 0, "Task editieren");
		menu.add(0, CONTEXT_DELETE_TASK, 0, "Task löschen");
	}

	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info;

		try {
			info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		} catch (ClassCastException e) {
			Log.e(TAG, "bad menuInfo", e);
			return false;
		}

		AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item
				.getMenuInfo();

		switch (item.getItemId()) {
		case CONTEXT_EDIT_TASK:
			Uri uri = ContentUris.withAppendedId(getIntent().getData(),
					menuInfo.id);
			System.out.println(menuInfo.position);
			Intent myIntent = new Intent(TaskaLaVista.this, TaskEditor.class);
			myIntent.setAction(Intent.ACTION_EDIT);
			myIntent.setData(uri);
			TaskaLaVista.this.startActivity(myIntent);

			Toast toast = Toast.makeText(TaskaLaVista.this, "Editiere Task",
					Toast.LENGTH_LONG);
			toast.show();
			// To get the id of the clicked item in the list use menuInfo.id
			Log.d(TAG, "list pos:" + menuInfo.position + " id:" + menuInfo.id);

			break;

		case CONTEXT_VIEW_TASK:
			ViewTask(menuInfo.position, menuInfo.id);
			break;

		case CONTEXT_DELETE_TASK:
			Uri taskUri = ContentUris.withAppendedId(getIntent().getData(),
					info.id);
			getContentResolver().delete(taskUri, null, null);

			Toast toast_deleted = Toast.makeText(TaskaLaVista.this,
					"Task wurde gelöscht", Toast.LENGTH_SHORT);
			toast_deleted.show();
			break;

		default:
			return super.onContextItemSelected(item);
		}
		return true;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		ViewTask(position, id);

	}

	private void ViewTask(int position, long id) {

		Uri uri = ContentUris.withAppendedId(getIntent().getData(), id);
		System.out.println(position);
		Intent myIntent = new Intent(TaskaLaVista.this, TaskViewer.class);
		myIntent.setAction(Intent.ACTION_VIEW);
		myIntent.setData(uri);
		TaskaLaVista.this.startActivity(myIntent);

	}

}